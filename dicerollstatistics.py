''' Analyzing dice rolls is a common example in understanding probability and statistics.
The following program calculates the number of times the sum of two dice (randomly rolled) is equal to six or seven.
zyDE 4.13.1: Dice statistics.
'''

import random
num_rolls = int(input('Enter number of rolls (enter 0 to quit):\n'))
possible = {
    '2': 0, '3': 0, '4': 0, '5': 0,
    '6': 0, '7': 0, '8': 0, '9': 0,
    '10': 0, '11': 0, '12': 0
}
while num_rolls > 1:
    if num_rolls >= 150:
        print('We haven\'t got all day... Try a lower number')
    elif num_rolls >= 1:
        for i in range(num_rolls):
            die1 = random.randint(1,6)
            die2 = random.randint(1,6)
            roll_total = die1 + die2
            possible[str(roll_total)] += 1
            print('Roll {} is {} ({} + {})'.format(i, roll_total, die1, die2))
        #now print the statistics for each possible outcome
        print('\nDice roll statistics:')
        for outcome in possible:
            print((outcome + 's:'), possible[outcome], (':' + (possible[outcome] * '*')))
    else:
        print('Invalid number of rolls. Try again.')



'''

Create a different version of the program that:

1    Calculates the number of times the sum of the randomly rolled dice equals each possible value from 2 to 12.
2    Repeatedly asks the user for the number of times to roll the dice, quitting only when the user-entered number is 
    less than 1. Hint: Use a while loop that will execute as long as num_rolls is greater than 1.
3    Prints a histogram in which the total number of times the dice rolls equals each possible value is displayed by 
    printing a character, such as *, that number of times. The following provides an example:

Dice roll histogram:

2s:  **
3s:  ****
4s:  ***
5s:  ********
6s:  *************
7s:  *****************
8s:  *************
9s:  *********
10s: **********
11s: *****
12s: **

'''
